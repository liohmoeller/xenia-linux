#!/bin/bash

TARGET_DRIVE=/dev/vda
STAGE4="http://45.12.146.142:8000/root.img"

wipefs -a $TARGET_DRIVE

[ -d "/sys/firmware/efi" ] && cat <<EOF | sfdisk --wipe always --force $TARGET_DRIVE
label: gpt
size=512M, type=uefi
type=lvm
EOF
[ ! -d "/sys/firmware/efi " ] && cat <<EOF | sfdisk --wipe always --force $TARGET_DRIVE
label: dos
size=512M, type=c
type=lvm
EOF


mkfs.vfat -F 32 ${TARGET_DRIVE}1
fatlabel ${TARGET_DRIVE}1 BOOT

yes | pvcreate -ff ${TARGET_DRIVE}2
umount /mnt/{boot,roots,overlay,root}
yes | lvremove -ff /dev/vg0
yes | vgcreate -ff vg0 ${TARGET_DRIVE}2
yes | lvcreate -L 8G -n roots vg0
yes | lvcreate -l 50%FREE -n overlay vg0
yes | lvcreate -l 100%FREE -n home vg0

yes | mkfs.ext4 /dev/vg0/roots -L ROOTS
yes | mkfs.ext4 /dev/vg0/overlay -L OVERLAY
yes | mkfs.ext4 /dev/vg0/home -L HOME

mkdir /mnt/{boot,roots,overlay,root,home}

mount --label BOOT /mnt/boot
mount --label ROOTS /mnt/roots
mount --label OVERLAY /mnt/overlay
mount --label HOME /mnt/home

mkdir /mnt/overlay/{var,varw}
mkdir /mnt/home/xenia

cd /mnt/roots
rm root.img
wget $STAGE4

mount -o ro,loop -t squashfs /mnt/roots/root.img /mnt/root
cp -r /mnt/root/boot/* /mnt/boot/

mount "${TARGET_DRIVE}1" /mnt/root/boot
mount -t proc /proc /mnt/root/proc
mount --rbind /dev /mnt/root/dev
mount --rbind /sys /mnt/root/sys
mount --bind /run /mnt/root/run
mount --make-slave /mnt/root/run

mount --move /mnt/home /mnt/root/home

chroot /mnt/root /bin/bash <<"EOT"
grub-install --target="x86_64-efi" --efi-directory="/boot"
grub-mkconfig -o /boot/grub/grub.cfg
chown xenia:xenia /home/xenia
EOT

umount -a
reboot
