# Xenia Linux rootFS

The Xenia Linux rootFS is a standard rootFS, but as a squashFS. The document will provide instructions on building a rootFS, before distribution of rootFS images begin.

> Disclaimer: Xenia Linux is in very, very early stages of development. This instructions may not work or may result in a broken system.

## Making a rootFS

### Creating the root

#### Automatic - old

Go to [this repo](https://gitlab.com/xenia-group/catalyst), inspect the `build.sh` and build the catalyst spec files. Make sure to put the `config` dir in your `/var/tmp/catalyst/config` dir, or symlink it.

#### Automatic - new

> Note: while we use foxpkg to build our root images, it's not very nice code (in fact it's actually quite horrific and needs a re-write, as it was intended to be a package manager of sorts for Xenia). If it doesn't work for you, feel free to use the above method or ask for help in an issue.

Go to [foxpkg](https://gitlab.com/xenia-group/foxpkg), and run `foxpkg -d init`. This process will guide you through downloading a stage3 to the right place, and will then setup catalyst for you as well as building the stage1 through stage3.

With this process completed, run `mkdir ~/.foxpkg/output`. After this, you can then run `foxpkg -d upgrade`. This will build the stage4, which you can find in `~/.foxpkg/output/root_temp.img`.

 To install this, run the [installer](https://gitlab.com/xenia-group/xenia-linux) and change the `STAGE4` variable to be equal to where you host the stage4. In development, we use `python3 -m http.server` to run a small server that the installer can pull the root from.

This is how we build our own rootFS images, and is the way that custom rootFS images can be made (for example, deploying an image for a webserver). You can also use this method to build more up-to-date images, or images with testing/unstable packages.

#### Manual (deprecated)

> This part of the documentation is here for historical reasons only, and for those who do not have a gentoo system - please use catalyst, or use a gentoo chroot to install it!

To make a rootFS and package it, a usual rootFS needs to be built. To build a rootFS, follow the Gentoo Handbook on installing Gentoo, as a virtual machine or chroot. Do the install process as normal, no need to use LVM or to deviate from the handbook, apart from the fact that a bootloader is not necessary.

While still in your chroot, make sure to install `lvm2` and `squashfs-tools`. Run `rc-update add lvm boot` to enable LVM support at boot.

Next, install any programs wanted and setup the system with a non-root user if desired. 

##### Packaging the root

The rootFS for Xenia Linux is a squashFS. To package the rootFS, use this command. Change the first two directories (source, destination) as desired, although this command will work while still in the chroot (most cases):

```
mksquashfs / /root.img -wildcards -e 'dev/*' -e 'proc/*' -e 'sys/*' -e 'home/*' -e 'boot/*' -e 'roots/*' -e 'var/cache/binpkgs/*' -e 'var/cache/distfiles/*' -e 'var/log/*' -e 'var/lock/*' -e 'var/run/*' -e 'var/tmp/*' -e 'var/spool/*'
```

This command generates a squashFS, but removes any files in `/dev`, `/proc`, `/sys`, `/home`, `/boot`, `/roots`, and any unnecessary files in `/var`.
