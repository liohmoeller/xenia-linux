# DEPRECATION NOTICE

As of 2023-01-20 (before any initial release has been made), we now use dracut to provide a initramfs. We do not use this initramfs anymore. I'm keeping this up as it took me ages to create and I am quite proud of it. 

Note: This documentation is *also* outdated from the way the init actually works. I am not updating the documentation as **Xenia does not use this anymore**.

# Xenia Linux initramfs

As explained in the root of this project, the initramfs is customised to be able to decompress and boot Xenia from the squashfs file on the LVM thin "roots" LV. This directory contains the init script for the initramfs.

> Disclaimer: Xenia Linux is in very, very early stages of development. 

## Building

To build an initramfs, follow the guide [here](https://wiki.gentoo.org/wiki/Custom_Initramfs). The guide will walk-through the building and packaging of an initramfs. Instead of writing an init script like shown in the guide, use the init script found here. Also, make sure to include LVM support (by including an `lvm.static` binary, the guide covers this) as well as copy in a module tree into `/lib/modules` as the init script relies on loading drivers for your storage, squashFS, LVM thin and loop devices.

If you are not using virtIO drivers for your storage, edit the init script to load the storage drivers the system needs. 

Next, edit the init script to conform to the system's LV naming scheme. The init script found here assumes a volume group called `vg0`, and LV names of `roots` and `rootfs`, whichh is what any future installers/tools will use.

Make sure to create the following directories in your initramfs, as these are used as mount points:

- `/mnt/root`
- `/mnt/roots`
- `/mnt/tmp-root`

Once the initramfs looks good, you can build it with `find . -print0 | cpio --null --create --verbose --format=newc | gzip --best > ./initramfs.img`.

## Installing

To use the initramfs, move it to the `/boot` directory, either overwriting your current initramfs or as a new initramfs. Then, edit the bootloader configuration to use the initramfs if necessary.

## Troubleshooting

To troubleshoot the initramfs, add `init=/bin/sh` to the kernel boot arguments. This will make the initramfs drop to a shell after it has tried to mount and extract the rootFS.