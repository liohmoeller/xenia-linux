# Xenia Linux

Xenia Linux is an immutable OS based on Gentoo Linux. Xenia Linux boots from a dracut initramfs, and boots from a SquashFS (the root), much like how a LiveCD works.

We named Xenia Linux after the popular Linux mascot who lost to Tux.

> Note: Xenia Linux is currently in alpha stages! Please do not install on your main machine - and use a VM to try it out first.

## How do I get it?

Xenia Linux has a very primitive installer built, and we host our root images over at http://45.12.146.142:8000/

> Note: This URL is a temporary solution while we fix FTP access. The current root image is at `/root.img`, but this will eventually be `/releases/current/root.img`

> Note: This installer will completely wipe your drive and also will only work on UEFI systems currently (Xenia Linux 0.1 Zerda)

To install Xenia Linux, firstly download a LiveCD of your choice. The installer is made to be run from anywhere, so just use what you are comfortable with.

Secondly, download the installer by typing `wget https://gitlab.com/xenia-group/xenia-linux/-/raw/main/installer/install.sh`. This will download the installer to your machine.

Next, open the installer with your favourite text editor. You will need to change `TARGET_DRIVE` to be equal to where the drive is to install to. For example, for device `/dev/vda`:

```bash
#!/bin/bash

TARGET_DRIVE=/dev/vda
STAGE4="http://45.12.146.142:8000/root.img"
```

Mark the installer as executable with `chmod +x install.sh`, and run it. You will see a lot of errors/warnings in this process, that is fine. Once done, your system will reboot into Xenia.

Both users (xenia and root) have a default password of `87658765XeniaLinux`.

### How do I install programs?

Be warned! Xenia operates much like a LiveCD and any programs installed with emerge will not persist on reboot. So then, how do you actually install anything?

Flatpak comes pre-installed. Firstly, install the Flathub repo by typing `flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo` into a terminal. Then, you can install apps with `flatpak install --user [application]`.

Currently, no container runtime is installed. In later version, docker will be shipped to allow users to run containers to install CLI applications.

If you want to install something such as libvirt, you can do so by producing your own rootFS using [foxpkg](https://gitlab.com/xenia-group/foxpkg).

## Overview of Xenia Linux

### Partition layout - physical

First partition - boot/EFI. Standard EFI partition containing kernel, initramfs and grub. Formatted as FAT32.

Second partition - LVM.

### Partition layout - logical

We use LVM for our layout. The volume group is called vg0.

roots - The LV which stores the squashFS rootFS images. EXT4, ROOTS label.  

overlay - The LV mounted at /overlay. This contains the overlays for parts of the system that need to be writable (/var), and in the future will contain root overlays made by foxoverlay. EXT4, VAR label.  

home - The LV mounted at /home. EXT4, HOME label.  

### Filesystem overview

The most important file in Xenia Linux is the root.img, and this is stored in the roots LV. This file is a squashFS of a root filesystem.

The rootFS image does not include any files in /home, as those are stored on a seperate LV (for write access primarily).

The initramfs is the second most important file in Xenia Linux. The initramfs allows us to boot from this squashFS.

## A thank you

Thank you to [Jack](https://gitlab.com/ArykDev) (our wonderful co-founder), Immoloism and the many people over at [It Runs Gentoo](
https://discord.gg/2VCEjaXqEe) for help (and emotional support) while making this distro. It wouldn't be possible without them. - Luna

But more importantly, thank you to YOU for trying Xenia Linux. 

If you ever need help, feel free to place issues here. And if you want to contribute, you are more than welcome to give us ideas or even make merge requests if you want to contribute technically to the project!